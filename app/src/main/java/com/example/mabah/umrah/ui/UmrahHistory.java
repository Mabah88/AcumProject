package com.example.mabah.umrah.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.mabah.umrah.R;

/**
 * Created by Mabah on 5/28/16.
 */
public class UmrahHistory extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_umrah_history);
    }
}
