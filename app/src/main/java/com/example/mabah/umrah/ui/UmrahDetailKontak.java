package com.example.mabah.umrah.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.example.mabah.umrah.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Mabah on 5/27/16.
 */
public class UmrahDetailKontak extends AppCompatActivity {

    @Nullable @Bind(R.id.linear_kontak)
    LinearLayout linearLayout_kontak;

    @Nullable @Bind(R.id.kontak_input_notelp)
    EditText NoTelp;

    @Nullable @Bind(R.id.kontak_input_email)
    EditText kontak_email;

    @Nullable @Bind(R.id.kontak_input_alamat)
    EditText kontak_alamat;

    @OnClick(R.id.detail_btn_kontak)
    void daftar(){

        String Notel = NoTelp.getText().toString();
        String Kontak = kontak_email.getText().toString();
        String Alamat = kontak_alamat.getText().toString();

        if (Notel.equals("")||Kontak.equals("")||Alamat.equals("")){
            final Snackbar snackbar = Snackbar.make(linearLayout_kontak,"Harap Lengkapi Data...!",Snackbar.LENGTH_LONG);
            snackbar.setAction("Tutup", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    snackbar.dismiss();
                }
            });
            snackbar.show();
        }else if (!isEmailValid(Kontak)){
            kontak_email.setError(getString(R.string.email_not_valid));
            kontak_email.requestFocus();
        }


    }

    private boolean isEmailValid(String kontak) {
        return kontak.contains("@");
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_umrah_detail_kontak);
        ButterKnife.bind(this);

    }

}
