package com.example.mabah.umrah.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.example.mabah.umrah.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Mabah on 5/26/16.
 */
public class UmrahRegisterMember extends AppCompatActivity {

    @Nullable @Bind(R.id.linear_register)
    LinearLayout linearLayout_register;

    @Nullable @Bind(R.id.register_input_name)
    EditText input_name;

    @Nullable @Bind(R.id.register_input_email)
    EditText input_email;

    @Nullable @Bind(R.id.register_input_password)
    EditText input_password;

    @Nullable @Bind(R.id.register_confirm_password)
    EditText input_confirm_password;

    @OnClick(R.id.register_btn_signup)
    void register(){

        String Nama = input_name.getText().toString();
        String Email = input_email.getText().toString();
        String Password = input_password.getText().toString();
        String Confirm = input_confirm_password.getText().toString();

        if (Nama.equals("")||Email.equals("")||Password.equals("")||Confirm.equals("")){
            final Snackbar snackbar = Snackbar.make(linearLayout_register,"Harap Lengkapi Data...!",Snackbar.LENGTH_LONG);
            snackbar.setAction("Tutup", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    snackbar.dismiss();
                }
            });
            snackbar.show();
        } else if (!isEmailValid(Email)){
            input_email.setError(getString(R.string.email_not_valid));
            input_email.requestFocus();

        }
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_umrah_register);
        ButterKnife.bind(this);
    }

}
