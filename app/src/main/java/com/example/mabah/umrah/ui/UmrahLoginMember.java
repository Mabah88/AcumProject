package com.example.mabah.umrah.ui;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.example.mabah.umrah.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UmrahLoginMember extends AppCompatActivity {

    @Nullable @Bind(R.id.linear)
    LinearLayout linearLayout;

    @Nullable @Bind(R.id.input_name)
    EditText nama;

    @Nullable @Bind(R.id.login_input_password)
    EditText login;

    @Nullable @OnClick(R.id.login_textView)
    void click(){

        Intent i = new Intent(getApplicationContext(),UmrahRegisterMember.class);
        startActivity(i);

    }

    @Nullable @OnClick(R.id.login_btn_signup)
    void mylogin() {
        String Nama = nama.getText().toString();
        String Password = login.getText().toString();
        if (Nama.equals("")||Password.equals("")){
            final Snackbar snackbar = Snackbar.make(linearLayout,"Harap Lengkapi Data...!",Snackbar.LENGTH_LONG);
            snackbar.setAction("Tutup", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    snackbar.dismiss();
                }
            });
            snackbar.show();
        }

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_umrah_login);
        ButterKnife.bind(this);
    }
}
