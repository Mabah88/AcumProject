package com.example.mabah.umrah.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.example.mabah.umrah.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Mabah on 5/26/16.
 */
public class UmrahDetailJamaah extends AppCompatActivity {

    @Nullable @Bind(R.id.linear_detail_jamaah)
    LinearLayout linearLayout_detail_jamaah;

    @Nullable @Bind(R.id.detail_input_name)
    EditText detail_name;

    @Nullable @Bind(R.id.detail_input_ktp)
    EditText detail_ktp;

    @Nullable @Bind(R.id.detail_input_jk)
    EditText detail_jk;

    @Nullable @Bind(R.id.detail_tempat_lahir)
    EditText detail_tempat_lahir;


    @Nullable @Bind(R.id.detail_tanggal_lahir)
    EditText detail_tanggal_lahir;

    @Nullable @OnClick(R.id.detail_btn_sign)
    void daftar(){

        String Nama = detail_name.getText().toString();
        String Ktp = detail_ktp.getText().toString();
        String Jk = detail_jk.getText().toString();
        String Tempat_lahir = detail_tempat_lahir.getText().toString();
        String Tanggal_lahir = detail_tanggal_lahir.getText().toString();

        if (Nama.equals("")|| Ktp.equals("")||Jk.equals("")||Tempat_lahir.equals("")||Tanggal_lahir.equals("")){
            final Snackbar snackbar = Snackbar.make(linearLayout_detail_jamaah,"Harap Lengkapi Data...!",Snackbar.LENGTH_LONG);
            snackbar.setAction("Tutup", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    snackbar.dismiss();
                }
            });
            snackbar.show();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_umrah_detail_jamaah);
        ButterKnife.bind(this);

    }
}
